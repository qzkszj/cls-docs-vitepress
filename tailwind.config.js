/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./components/**/*.{vue, js, ts, tsx}"
  ],
  theme: {
    extend: {},
  },
  plugins: [],
}

