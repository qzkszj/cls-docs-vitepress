<script setup lang="ts">
import PNATable from '../../components/pnatable.vue'
</script>

# PNA

## 觉醒机制

| 工具 | 成功率提升 | 材料消耗 | 期望消耗 |
| ---- | ---------- | ------------ | ------------ |
| 完美的PNA进化工具 | 固定+25% | -30% | -52% |
| 进化促进向导 | 固定+10% | \ | -15% |
| 进化促进剂 | \ | -10% | -10% |

:::details 觉醒因子强化材料消耗明细表
<PNATable />
:::

## 表现型

提供大量类型伤和重铸基础

每到达一个特定等级可以获得一个8种不同类型的重铸槽，每种类型不同等级的[重铸概率](https://discord.com/channels/884263501974097931/884730736479981588/1221734018815627264):

| C | B | A | S | SS |
| - | - | - | - | -- |
| 2.7% | 5% | 3.75% | 1% | 0.05% |

每个PNA进化工具提供10次重铸机会。

[韩服随机概率公示](http://closers.nexon.com/News/Guide/View?n4ArticleSN=235&n4ArticleCategorySN=4)