# 各角色战斗指北（WIP）

- [详细的分角色攻略](https://closersinfo.xyz/new_ret_notice/)
- [技能加点参考表](https://docs.google.com/spreadsheets/d/1M1Qvw8TALxd2zZjTEd6Rq8p_UpCDswCcV-wf7tpDCrE/edit#gid=735145887)
- [技能固定伤害判定总览](https://docs.google.com/spreadsheets/d/1T3SC69WbNo_UUKiNUMYrwuF4SULZ6vklZfhy69vJalw/edit#gid=832602391)

上文三攻略均来自closersinfo社区

- [角色技能输出统计](https://www.bilibili.com/video/BV17V4y1U7pD)
    - 仅供参考，实际考虑技能释放优先级的时候要看技能形态和联动等等问题
        - e.g. 白的ex踏雪斩秒伤虽然很高但释放很墨迹，如果和突进斩同时回转完毕肯定先放施法快的突进斩

新角色考虑点：

- 关联型冷却缩减
- 无敌/减伤技能
- buff技能
- 后摇取消方式

## buff指南

### 李世赫

叠buff: 苍穹-炸裂-ex空爆弹-超新星-超能觉醒-输出

:::details buff/回复技
| 技能 | buff |
| ---- | ---- |
| 苍穹震击（觉醒） | 5%总攻 50%物爆 6%攻速 **回蓝** |
| ex空爆弹 | (50x+50)攻击 (0.95x+4.05)%物理伤害增加 |
| 超新星   | 5%总攻 10%移速 30%空/背爆 (100x+400)攻击(常驻) |
| 炸裂     | 15%减抗 |
| ex园震冲击 | 10%**回蓝** |
:::

:::details 减伤/无敌技
| 技能 | 减伤/无敌 |
| ---- | --------- |
| 苍穹震击 | 无敌 |
| 爆灵剑天焚[新觉] | 无敌 |
| 疾冲[修炼] | 无敌（准备动作）/80减（疾冲过程） |
| ex空爆弹 | 80%减 |
| 火碎(未强化状态) | 80%减 |
| 流星剑 | 80%减 |
| 炸裂 | 40%减 |
| 昊天 | TODO |
:::

### 李雪菲

EXD回蓝，ex重力场填满戒律计数器，3碎片消耗技刷新虫洞cd

### 缇娜

10适应战场：狂热-狂热-幻影狙击-狂热-狂热-火力全开-三转大

主要buff: 0转大 3转大 10适应战场

### 白

主要buff：冻结 开启剑幕 冰封热泪
