---
sidebar_position: 1
slug: /
---

# 欢迎

## 关于

使用vitepress管理内容，cloudflare pages实现持续部署，感谢开发者们的付出。

本篇主要记录本人在游戏《封印者》上遇到过的困惑，也希望作为中文区的一份数值指南和模型分析，对现有攻略进行补充。如果有误，麻烦大家提PR指正，谢谢大家。  

不同于其它的攻略，该笔记以网站的形式公布的同时，还发布了markdown源代码(见页面右上角菜单)，希望能够支援攻略区的发展。

同时，本篇并不完善，如果需要系统的数据百科，更建议使用下面的友链closersinfo。  

点击左侧菜单上的栏目以阅读本文档。  
作者处于半退坑状态，请注意文档时效性。  

给新人的建议：看本文档前过一遍下面任一wiki的“回归/新手指南”。  

我向天发誓，正文中绝对不会出现这该死的翻译腔。

## 友链

### wiki

- [Closers info（日语）](https://closersinfo.jp/) 非常系统，基础数值全
- [BWiki](https://wiki.biligame.com/closers/%E9%A6%96%E9%A1%B5)  本地化
- [closersinfo.xyz（繁中）](https://closersinfo.xyz/) 台人做的wiki，相比jp有服装，爆抗数据和更准确的数值理解
- [namu.wiki(韩文)](https://namu.wiki) 看上去很厉害 但还不会用


### 专题

- [韩服官网概率资料汇总](https://closers.nexon.com/News/Guide/List?n4ArticleCategorySN=4)
    - 进官网后点击右侧菜单的百分号图标即可进入该界面
- [220805 角色特点强度大致整理](https://tieba.baidu.com/p/7960148838)
- [220911 白PVE攻略](https://tieba.baidu.com/p/8016335815)
    - [PVE相关心得](https://docs.qq.com/doc/DWkpRbFhmZ0JPQU5Q)
    - [主动技能说明](https://docs.qq.com/sheet/DWldzUUhKd0hERXZV?tab=BB08J2)

### 攻略者们

- [芝麻爱花花：更新详解：机制与数值百科](https://space.bilibili.com/514107)
- [千坂雪樱：萌新入坑基础教程/Boss流程讲解](https://space.bilibili.com/7418277)
- [迷途の塔塔：数值科普/硬件科普/低配战术家](https://space.bilibili.com/385077345)
- [镜游御影：数值理解/方向教学](https://space.bilibili.com/1499829)
- [傲娇娇official：数值测试/关卡流程分析](https://space.bilibili.com/7782025)
