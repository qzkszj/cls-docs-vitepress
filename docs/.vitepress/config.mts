import { defineConfig } from 'vitepress'
// import markdownItContainer from 'markdown-it-container'

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: "CLOSERS 数值指北",
  description: "closers 数值指北",
  head: [
    ['link', { rel: 'icon', href: '/favicon.svg' }],
  ],
  lastUpdated: true,
  sitemap: {
    hostname: 'https://cls.qzkszj.top'
  },
  markdown: {
    math: true,
    image: {
      lazyLoading: true,
    },
    // config: (md) => {
    //   md.use(markdownItContainer)
    // },
  },
  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    outline: [2, 3],
    nav: [
      { text: 'Home', link: '/' },
    ],
    search: {
      provider: "local",
      options: {
        miniSearch: {
          options: {

          },
          searchOptions: {

          },
        }
      }
    },

    sidebar: [
      { text: '欢迎', link: '/' },
      {
        text: '数值基础与伤害组成',
        items: [
          { text: '前置概念', link: '/numeric/prologue' },
          { text: '伤害组成', link: '/numeric/damage' },
          { text: '角色面板', link: '/numeric/character' },
        ],
        collapsed: false,
      },
      {
        text: '战力提升系统',
        items: [
          { text: '装备，超越与强化', link: '/reinforcement/reinforcement' },
          { text: '芯片', link: '/reinforcement/chips' },
          { text: '时装', link: '/reinforcement/costume' },
          { text: '重铸', link: '/reinforcement/tuning' },
          { text: 'PNA', link: '/reinforcement/pna' },
          { text: '增益', link: '/reinforcement/buff' },
        ],
        collapsed: false,
      },
      {
        text: '副本基础信息',
        link: '/region',
      },
      {
        text: '刷图产出参考',
        link: '/drops',
      },
      {
        text: '分角色战斗指北(WIP)',
        link: '/skills',
      },
      {
        text: '装备制作指南(deprecated)',
        link: '/equipments',
      },
      /*
      {
        text: 'Examples',
        items: [
          { text: 'api-examples', link: '/api-examples' },
          { text: 'markdown-examples', link: '/markdown-examples' }
        ],
        collapsed: false,
      }
        */
    ],

    socialLinks: [
      { icon: 'github', link: 'https://gitlab.com/qzkszj/cls-docs-vitepress' }
    ]
  }
})


// import * as fs from 'fs';
// import * as path from 'path';
// import * as matter from 'gray-matter'
// 
// function getSideBar(folder, titleFromMatter = 1) {
//   const extension = [".md"];
// 
//   const files = fs
//     .readdirSync(path.join(`${__dirname}/../${srcDir}/${folder}`))
//     .filter(
//       (item) =>
//         item.toLowerCase() != "readme.md" &&
//         fs.statSync(path.join(`${__dirname}/../${srcDir}/${folder}`, item)).isFile() &&
//         extension.includes(path.extname(item))
//     )
//     .map((item) => {
//       let text = titleFromMatter?
//         matter.read(path.join(`${__dirname}/../${srcDir}/${folder}`, item)).data?.title
//         : undefined
//       if(text === undefined){
//         text = item.substring(0, item.lastIndexOf('.'))
//       }
//       return {
//         text,
//         "link": path.join(`${folder}`, item.substring(0, item.lastIndexOf('.')))
//       }
//     })
// 
//   // console.log(files)
//   return files
// }
